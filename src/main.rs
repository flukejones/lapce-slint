use std::{fs::File, path::PathBuf};

use anyhow::Result;
use flate2::read::GzDecoder;
use lapce_plugin::{
    psp_types::{
        lsp_types::{request::Initialize, DocumentFilter, InitializeParams, MessageType, Url},
        Request,
    },
    register_plugin, Http, LapcePlugin, PLUGIN_RPC,
};
use serde_json::Value;

#[derive(Default)]
struct State {}

register_plugin!(State);

fn initialize(params: InitializeParams) -> Result<()> {
    let document_selector = vec![DocumentFilter {
        language: Some("slint".to_string()),
        pattern: Some(String::from("**/*.slint")),
        scheme: None,
    }];
    let mut server_args = vec![];
    let mut server_path = None;

    if let Some(options) = params.initialization_options.as_ref() {
        server_path = options
            .get("serverPath")
            .and_then(|server_path| server_path.as_str())
            .and_then(|server_path| {
                if !server_path.is_empty() {
                    Some(server_path)
                } else {
                    None
                }
            });

        if let Some(args) = options.get("serverArgs") {
            if let Some(args) = args.as_array() {
                for arg in args {
                    if let Some(arg) = arg.as_str() {
                        server_args.push(arg.to_string());
                    }
                }
            }
        }
    }

    if let Some(server_path) = server_path {
        let program = match std::env::var("VOLT_OS").as_deref() {
            Ok("windows") => "where",
            _ => "which",
        };
        let exists = PLUGIN_RPC
            .execute_process(program.to_string(), vec![server_path.to_string()])
            .map(|r| r.success)
            .unwrap_or(false);
        if !exists {
            PLUGIN_RPC.window_show_message(
                MessageType::ERROR,
                format!("server path {server_path} couldn't be found, please check"),
            );
            return Ok(());
        }
        PLUGIN_RPC.start_lsp(
            Url::parse(&format!("urn:{}", server_path))?,
            server_args,
            document_selector,
            params.initialization_options,
        );
        return Ok(());
    }

    let file_name = "slint-lsp".to_string();
    let file_path = PathBuf::from(&file_name);

    let archive_path = match std::env::var("VOLT_OS").as_deref() {
        Ok("linux") => PathBuf::from(file_name.clone() + "-linux.tar.gz"),
        Ok("windows") => PathBuf::from(file_name.clone() + "-windows.zip"),
        _ => {
            PLUGIN_RPC.window_show_message(
                MessageType::ERROR,
                "can't download slint-lsp, unsupported arch or OS".to_string(),
            );
            return Ok(());
        }
    };

    if !file_path.exists() {
        let result: Result<()> = {
            let url = format!(
                "https://github.com/slint-ui/slint/releases/download/v1.5.0/{}",
                archive_path.to_string_lossy()
            );
            {
                let mut resp = Http::get(&url)?;
                PLUGIN_RPC.stderr(&format!("STATUS_CODE: {:?}", resp.status_code));
                let body = resp.body_read_all()?;
                std::fs::write(&archive_path, body)?;
                let mut gz = GzDecoder::new(File::open(&archive_path)?);
                let mut file = File::create(&file_path)?;
                std::io::copy(&mut gz, &mut file)?;
            }
            std::fs::remove_file(&archive_path)?;
            Ok(())
        };
        if result.is_err() {
            PLUGIN_RPC.window_show_message(
                MessageType::ERROR,
                "can't download slint-lsp, please use server path in the settings.".to_string(),
            );
            return Ok(());
        }
    }

    let volt_uri = std::env::var("VOLT_URI")?;
    let server_path = Url::parse(&volt_uri)?.join(&file_name)?;
    PLUGIN_RPC.start_lsp(
        server_path,
        server_args,
        document_selector,
        params.initialization_options,
    );
    Ok(())
}

impl LapcePlugin for State {
    fn handle_request(&mut self, _id: u64, method: String, params: Value) {
        #[allow(clippy::single_match)]
        match method.as_str() {
            Initialize::METHOD => {
                let params: InitializeParams = serde_json::from_value(params).unwrap();
                if let Err(e) = initialize(params) {
                    PLUGIN_RPC.stderr(&format!("plugin returned with error: {e}"))
                }
            }
            _ => {}
        }
    }
}
